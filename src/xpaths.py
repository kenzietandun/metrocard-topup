#!/usr/bin/env python3


username_input_xpath = '//*[@id="formly_1_integer-noLabel_metrocardNo_0"]'
password_input_xpath = '//*[@id="formly_1_password-noLabel_password_0"]'
login_button_xpath = "/html/body/div[1]/ui-view/div/div/div/div/div/div[2]/ng-transclude/ui-view/div/form/div[2]/input"

greeting_text_xpath = "/html/body/div[1]/ui-view/div/div/section/section/div/ng-transclude/div/metrocard-site-section/div/div/div/ng-transclude/div/div/div/div/div/ng-transclude/div[1]/h2"
current_balance_xpath = "/html/body/div[1]/ui-view/div/div/section/section/div/ng-transclude/div/metrocard-site-section/div/div/div/ng-transclude/div/div/div/div/div/ng-transclude/metrocard-card-details/div/div[2]/span"

top_up_button_xpath = "/html/body/div[1]/ui-view/div/div/section/section/div/ng-transclude/div/metrocard-site-section/div/div/div/ng-transclude/div/div/div/ng-transclude/form/input"
top_up_checkout_xpath = "/html/body/div[1]/ui-view/div/div/section/section/div/ng-transclude/div/metrocard-site-section/div/div/div/ng-transclude/div/div/div/ng-transclude/p/metrocard-site-text/span/stp-display-html/span/span/p[2]"

payment_header_xpath = "/html/body/div[2]/form/div[2]/div[1]/h2"
payment_credit_card_xpath = '//*[@id="36864065"]'
payment_credit_name_xpath = '//*[@id="36864066"]'
payment_credit_expiry_month_xpath = '//*[@id="DateExpiry_1"]'
payment_credit_expiry_year_xpath = '//*[@id="DateExpiry_2"]'
payment_credit_cvc_xpath = '//*[@id="36864069"]'
