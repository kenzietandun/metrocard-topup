#!/usr/bin/env python3

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from credentials import email_username, email_password
from string import Template

TEMPLATE_MSG = """
Dear ${PERSON},

Your current balance is $$${BALANCE}.

"""

def send_email(sender="", to="", person="", balance=0.0):
    s = smtplib.SMTP(host="smtp.gmail.com", port=587)
    s.starttls()
    s.login(email_username, email_password)

    msg = MIMEMultipart()
    msg["From"] = sender
    msg["To"] = to
    msg["Subject"] = "Your current Metrocard balance"

    msg_body = Template(TEMPLATE_MSG).substitute(PERSON=person, BALANCE=balance)
    msg.attach(MIMEText(msg_body, "plain"))

    s.send_message(msg)
    s.quit()
